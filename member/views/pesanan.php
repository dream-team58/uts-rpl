<?php
    $query = mysqli_query($koneksi, "SELECT * FROM transaksi WHERE status < '4' AND id_member = '$_SESSION[id]' ORDER BY status ASC, tgl_transaksi ASC");
?>

<div class="row">
    <div class="col-xl-11">
        <div class="row">
            <div class="col-auto text-center flex-column d-none d-sm-flex">
                <div class="row h-50"></div>
                <h5 class="m-2">
                    <span class="badge badge-pill bg-success border">&nbsp;</span>
                </h5>
                <div class="row h-50">
                    <div class="col border-right">&nbsp;</div>
                    <div class="col">&nbsp;</div>
                </div>
            </div>
            <div class="col py-2">
                <div class="card bg-primary text-white">
                    <div class="card-body">
                        <div class="float-right text-muted">Wed, Jan 11th 2019 8:30 AM</div>
                        <h4 class="card-title"><b>Ekspress</b></h4>
                        <p>
                            Shoreditch vegan artisan Helvetica. Tattooed Codeply Echo Park Godard kogi, next level irony ennui twee squid fap selvage. Meggings flannel Brooklyn literally small batch, mumblecore PBR try-hard kale chips.
                            Brooklyn vinyl lumbersexual bicycle rights, viral fap cronut leggings squid chillwave pickled gentrify mustache. 3 wolf moon hashtag church-key Odd Future. Austin messenger bag normcore, Helvetica Williamsburg
                            sartorial tote bag distillery Portland before they sold out gastropub taxidermy Vice.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        
        <?php while ($row = mysqli_fetch_assoc($query)): ?>
            <div class="row">
                <div class="col-auto text-center flex-column d-none d-sm-flex">
                    <div class="row h-50">
                        <div class="col border-right">&nbsp;</div>
                        <div class="col">&nbsp;</div>
                    </div>
                    <h5 class="m-2">
                        <span class="badge badge-pill bg-success border">&nbsp;</span>
                    </h5>
                    <div class="row h-50">
                        <div class="col border-right">&nbsp;</div>
                        <div class="col">&nbsp;</div>
                    </div>
                </div>
                <div class="col-lg-8 py-2">
                    <div class="card">
                        <div class="card-body">
                            <div class="float-right"> 
                                <?php if ($row['status'] == '0') echo "<span class=\"badge badge-warning\">Menunggu</span>" ?>
                                <?php if ($row['status'] == '1') echo "<span class=\"badge badge-info\">Dikerjakan</span>" ?>
                                <?php if ($row['status'] == '2') echo "<span class=\"badge badge-primary\">Selesai Dikerjakan</span>" ?>
                                <?php if ($row['status'] == '3') echo "<span class=\"badge badge-secondary\">Sedang Diantarkan</span>" ?>
                                <?php if ($row['status'] == '4') echo "<span class=\"badge badge-success\">Selesai</span>" ?>
                                (<?= date('d M Y, H:i:s', strtotime($row['update_at'])) ?>)
                            </div>
                            <h4 class="card-title text-primary"><b><?= $row['kode_transaksi'] ?></b></h4>
                            <p>Shoreditch vegan artisan Helvetica</p>
                            <p class="card-text">Sign-up for the lessons and speakers that coincide with your course syllabus. Meet and greet with instructors.</p>
                            <button class="btn btn-sm btn-outline-success" type="button" data-target="#<?= $row['kode_transaksi'] ?>" data-toggle="collapse">Detail Pesanan</button>
                            <div class="collapse border" id="<?= $row['kode_transaksi'] ?>">
                                <div class="p-2">

                                <?php $query_detail = mysqli_query($koneksi, "SELECT detail_transaksi.nama_produk, detail_transaksi.qty, produk.icon, produk.harga AS harga_satuan FROM detail_transaksi JOIN produk ON detail_transaksi.id_produk = produk.id_produk WHERE kode_transaksi = '$row[kode_transaksi]'");
                                            while ($row_detail = mysqli_fetch_assoc($query_detail)): ?>
                                            
                                            <div class="col-xs-12 col-sm-6 mt-2 p-2 border-left-after border-bottom-after-xs">
                                                <div class="avatar-lg float-left mr-2">
                                                    <img src="<?= '../' . $row_detail['icon'] ?>" alt="<?= $row_detail['nama_produk'] ?>" class="avatar-img">
                                                </div>
                                                <div class="info">
                                                    <span>
                                                        <h5 class="mb-0 mt-1"><?= $row_detail['nama_produk'] ?></h5>
                                                        <span class="user-level mt-0"><?= $row_detail['qty'] . ' @ ' . $row_detail['harga_satuan'] ?></span>
                                                    </span>
                                                </div>
                                            </div>
                                                
                                        <?php endwhile; ?>










                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>


        <div class="row">
            <div class="col-auto text-center flex-column d-none d-sm-flex">
                <div class="row h-50">
                    <div class="col border-right">&nbsp;</div>
                    <div class="col">&nbsp;</div>
                </div>
                <h5 class="m-2">
                    <span class="badge badge-pill bg-success border">&nbsp;</span>
                </h5>
                <div class="row h-50"></div>
            </div>
            <div class="col py-2">
                <div class="card">
                    <div class="card-body">
                        <div class="float-right text-muted">Wed, Jan 11th 2019 8:30 AM</div>
                        <h4 class="card-title"><b>Ekspress</b></h4>
                        <p>
                            Shoreditch vegan artisan Helvetica. Tattooed Codeply Echo Park Godard kogi, next level irony ennui twee squid fap selvage. Meggings flannel Brooklyn literally small batch, mumblecore PBR try-hard kale chips.
                            Brooklyn vinyl lumbersexual bicycle rights, viral fap cronut leggings squid chillwave pickled gentrify mustache. 3 wolf moon hashtag church-key Odd Future. Austin messenger bag normcore, Helvetica Williamsburg
                            sartorial tote bag distillery Portland before they sold out gastropub taxidermy Vice.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>













                

                    
            </div>
        </div>
    </div>
</div>







<div class="row">
            <?php if (mysqli_num_rows($query) < 1): ?>
                <div class="col-12">
                    <div class="alert alert-danger fade show" role="alert">
                        <p>Belum ada catatan riwayat pesanan saat ini</p>
                    </div>
                </div>
            <?php else: ?>
                <?php while ($row = mysqli_fetch_assoc($query)): ?>

                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8">
                                        <dl>
                                            <dt class="text-uppercase text-muted font-weight-bolde">Kode Transaksi</dt>
                                            <dd><?= $row['kode_transaksi'] ?></dd>
                                        </dl>
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-outline-primary btn-sm btn-detail-transaksi float-right" data-toggle="modal" data-target="#detail-transaksi" data-kt="<?= base64_encode($row['kode_transaksi']) ?>">Detail</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8 col-xl-6 pr-1">
                                        <dl>
                                            <dt class="text-uppercase text-muted font-weight-bolde">Status Pengerjaan</dt>
                                            <dd>
                                                <?php if ($row['status'] == '0') echo "<span class=\"badge badge-warning\">Menunggu</span>" ?>
                                                <?php if ($row['status'] == '1') echo "<span class=\"badge badge-info\">Dikerjakan</span>" ?>
                                                <?php if ($row['status'] == '2') echo "<span class=\"badge badge-primary\">Selesai Dikerjakan</span>" ?>
                                                <?php if ($row['status'] == '3') echo "<span class=\"badge badge-secondary\">Sedang Diantarkan</span>" ?>
                                                <?php if ($row['status'] == '4') echo "<span class=\"badge badge-success\">Selesai</span>" ?>
                                                <!-- <br> -->
                                                <span class="d-inline">(<?= date('d M Y, H:i:s', strtotime($row['update_at'])) ?>)</span>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="col-4 col-xl-6 pl-1">
                                        <dl class="ml-auto d-inline text-right">
                                            <dt class="text-uppercase text-muted font-weight-bolde">Total</dt>
                                            <dd class=>Rp. <?= number_format($row['total'], 0, ',', '.') ?></dd>
                                        </dl>
                                    </div>
                                    <hr class="mt--2 mb-2">
                                </div>
                            </div>
                            <div class="card-footer bg-grey1">
                                <div class="card-title">Detail Produk</div>
                                <div class="row">
                                    <?php
                                        $query_detail = mysqli_query($koneksi, "SELECT detail_transaksi.nama_produk, detail_transaksi.qty, produk.icon, produk.harga AS harga_satuan FROM detail_transaksi JOIN produk ON detail_transaksi.id_produk = produk.id_produk WHERE kode_transaksi = '$row[kode_transaksi]'");
                                        while ($row_detail = mysqli_fetch_assoc($query_detail)): ?>
                                        
                                        <div class="col-xs-12 col-sm-6 mt-2 p-2 border-left-after border-bottom-after-xs">
                                            <div class="avatar-lg float-left mr-2">
                                                <img src="<?= '../' . $row_detail['icon'] ?>" alt="<?= $row_detail['nama_produk'] ?>" class="avatar-img">
                                            </div>
                                            <div class="info">
                                                <span>
                                                    <h5 class="mb-0 mt-1"><?= $row_detail['nama_produk'] ?></h5>
                                                    <span class="user-level mt-0"><?= $row_detail['qty'] . ' @ ' . $row_detail['harga_satuan'] ?></span>
                                                </span>
                                            </div>
                                        </div>
                                            
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>

                <?php endwhile; ?>
            <?php endif; ?>
        </div>